<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => '/', 'uses' => 'Backend\MasterController@index'])->middleware(['sentinel_auth', 'web']);

Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

Route::group(['middleware' => ['sentinel_auth', 'web']], function () {
    Route::get('/user', array('as' => 'user', 'uses' => 'Backend\UserController@index'));
    Route::get('/user/datatables', array('as' => 'user.datatables', 'uses' => 'Backend\UserController@datatable'));
    Route::post('/user/store', array('as' => 'user.store', 'uses' => 'Backend\UserController@store'));
    Route::get('/user/edit', array('as' => 'user.edit', 'uses' => 'Backend\UserController@edit'));
    Route::post('/user/update', array('as' => 'user.update', 'uses' => 'Backend\UserController@update'));

    Route::get('/product', array('as' => 'product', 'uses' => 'Backend\ProductController@index'));
    Route::get('/product/datatables', array('as' => 'product.datatables', 'uses' => 'Backend\ProductController@datatable'));
    Route::post('/product/store', array('as' => 'product.store', 'uses' => 'Backend\ProductController@store'));
    Route::get('/product/edit', array('as' => 'product.edit', 'uses' => 'Backend\ProductController@edit'));
    Route::post('/product/update', array('as' => 'product.update', 'uses' => 'Backend\ProductController@update'));

    Route::get('/purchase', array('as' => 'purchase', 'uses' => 'Backend\PurchaseController@index'));
    Route::get('/purchase/datatables', array('as' => 'purchase.datatables', 'uses' => 'Backend\PurchaseController@datatable'));
    Route::post('/purchase/store', array('as' => 'purchase.store', 'uses' => 'Backend\PurchaseController@store'));
    Route::get('/purchase/edit', array('as' => 'purchase.edit', 'uses' => 'Backend\PurchaseController@edit'));
    Route::post('/purchase/update', array('as' => 'purchase.update', 'uses' => 'Backend\PurchaseController@update'));
    
    Route::get('/report', array('as' => 'report', 'uses' => 'Backend\PurchaseController@report'));
    Route::get('/report/datatables', array('as' => 'report.datatables', 'uses' => 'Backend\PurchaseController@datatableReport'));
    Route::get('/report/download', array('as' => 'report.download', 'uses' => 'Backend\PurchaseController@download'));

    Route::get('/supplier', array('as' => 'supplier', 'uses' => 'Backend\SupplierController@index'));
    Route::get('/supplier/datatables', array('as' => 'supplier.datatables', 'uses' => 'Backend\SupplierController@datatable'));
    Route::post('/supplier/store', array('as' => 'supplier.store', 'uses' => 'Backend\SupplierController@store'));
    Route::get('/supplier/edit', array('as' => 'supplier.edit', 'uses' => 'Backend\SupplierController@edit'));
    Route::post('/supplier/update', array('as' => 'supplier.update', 'uses' => 'Backend\SupplierController@update'));
});