<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    public $timestamps = true;
    
    protected $fillable = ['name','satuan','category_id','supplier_id'];

    public function datatables()
    {
        return Product::select('id','name','satuan','category_id','supplier_id');
    }
}
