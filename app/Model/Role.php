<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    public $timestamps = true;
    
    protected $fillable = ['name','is_super_admin','slug','permission'];
}
