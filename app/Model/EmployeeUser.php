<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmployeeUser extends Model
{
    protected $table = 'employee_users';
    public $timestamps = true;
    
    protected $fillable = ['user_id','employee_id'];
    
    public function employee()
    {
        return $this->hasOne('App\Model\Employee', 'id', 'employee_id');
    }

    public function user()
    {
        return $this->hasOne('App\Model\User','id', 'user_id');
    }
}
