<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'suppliers';
    public $timestamps = true;
    
    protected $fillable = ['name','address','email','phone','tax_number','bank_code','bank_name','account_number','created_by'];

    public function datatables()
    {
        return Supplier::select('name','address','email','phone','tax_number','bank_code','bank_name','account_number','created_by');
    }
}
