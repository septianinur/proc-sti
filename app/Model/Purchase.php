<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $table = 'purchases';
    public $timestamps = true;
    
    protected $fillable = ['pic','supplier_id','purchase_date','due_date','receive_date','purchase_terms','product_id','total','memo','status'];

    public function datatables()
    {
        return Purchase::select('pic','supplier_id','purchase_date','due_date','receive_date','purchase_terms','product_id','total','memo','status');
    }
}
