<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Supplier;
use DataTables;

class SupplierController extends Controller
{
    public function index()
    {
        return view('backend.supplier.index');
    }

    public function datatable()
    {
        try {
            $model = new Supplier;
            $model =  $model->datatables();
        } catch (Exception $e) {
            return redirect()->route('/')->with('error', trans('label.error.internal_server_error'));
        }
        
        return DataTables::of($model)
            ->addIndexColumn()
            ->addColumn('action', function($data) {
                $edit = '<button type="button" data-url="'.$data->email.'
                " data-method="EDIT" class="btn btn-primary btn-xs" id="update-data" title="'.trans('label.update').'" style="margin-left: 5%">
                <i class="fa fa-pencil"></i></button>';
    
                return $edit;
            })
            ->make(true);
    }
}
