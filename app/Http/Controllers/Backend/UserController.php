<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Model\User;
use App\Model\Role;
use App\Model\Employee;
use App\Model\EmployeeUser;
use DataTables;

class UserController extends Controller
{
    public function index()
    {
        $data['roles'] = Role::select('id','name')->where('id','!=',1)->get();
        return view('backend.user.index', $data);
    }

    public function datatable()
    {
        try {
            $model = new User;
            $model =  $model->datatables();
        } catch (Exception $e) {
            return redirect()->route('/')->with('error', trans('label.error.internal_server_error'));
        }
        
        return DataTables::of($model)
            ->addIndexColumn()
            ->addColumn('action', function($data) {
                $edit = '<button type="button" data-url="'.$data->id.'
                " data-method="EDIT" class="btn btn-primary btn-xs" id="update-data" title="'.trans('label.update').'" style="margin-left: 5%">
                <i class="fa fa-pencil"></i></button>';
    
                return $edit;
            })
            ->addColumn('name', function ($data) {
                return empty($data->employee_user) ? '-' : $data->employee_user->employee->name;
            })
            ->addColumn('nik', function ($data) {
                return empty($data->employee_user) ? '-' : $data->employee_user->employee->nik;
            })
            ->addColumn('phone', function ($data) {
                return empty($data->employee_user) ? '-' : $data->employee_user->employee->phone;
            })->make(true);
    }

    public function store(Request $request)
    {
        try {
            $user = Sentinel::registerAndActivate([
                'is_admin' => false,
                'email' => $request->email,
                'password' => $request->password,
            ]);

            Sentinel::findRoleById($request->role)->users()->attach(Sentinel::findById($user->id));
            
            $employee = Employee::insert([
                'nik' => $request->nik,
                'name' => $request->name,
                'photo' => null,
                'phone' => $request->phone,
            ]);
            
            EmployeeUser::insert([
                'user_id' => $user->id,
                'employee_id' => $employee->id,
            ]);
            
            return redirect()->route('user');
        } catch (\Exception $e) {
            $status = 'error';
            $msg    = trans('label.error.internal_server_error');
            return redirect()->route('user')->with($status, $msg);
        }
    }
    
    public function edit(Request $request)
    {
        try {
            $model = new User;
            $user = $model->find($request->id);
            $user_id = $user->id;
            $user_email = $user->email;
            $user_nik = $user->employee_user->employee->nik;
            $user_name = $user->employee_user->employee->name;
            $user_phone = $user->employee_user->employee->phone;
            return response()->json(['id' => $user_id, 'email' => $user_email, 'nik' => $user_nik, 'name' => $user_name, 'phone' => $user_phone, 'status' => 'Success']);
        } catch (Exception $e) {      
            $status = 'error';
            $msg    = trans('label.error.internal_server_error');
            return response()->json(['status' => $status, 'message' => $msg]);
        }
    }

    public function update(Request $request)
    {
        try{
            $user = User::find($request->id)->update([
                'email' => $request->email,
            ]);
            
            $employee = Employee::find($user->employee_user->employee->id)->update([
                'nik' => $request->nik,
                'name' => $request->name,
                'phone' => $request->phone,
            ]);
            
            return redirect()->route('user');
        }catch (Exception $e) {   
            \DB::rollback();   
            $status = 'error';
            $msg    = trans('label.error.internal_server_error');
            return redirect()->route('location')->with($status, $msg);
        }
    }
}
