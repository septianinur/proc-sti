<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Purchase;
use DataTables;

class PurchaseController extends Controller
{
    public function index()
    {
        return view('backend.purchase.index');
    }

    public function datatable()
    {
        try {
            $model = new Purchase;
            $model =  $model->datatables();
        } catch (Exception $e) {
            return redirect()->route('/')->with('error', trans('label.error.internal_server_error'));
        }
        
        return DataTables::of($model)
            ->addIndexColumn()
            ->addColumn('action', function($data) {
                $edit = '<button type="button" data-url="'.$data->id.'
                " data-method="EDIT" class="btn btn-primary btn-xs" id="update-data" title="'.trans('label.update').'" style="margin-left: 5%">
                <i class="fa fa-pencil"></i></button>';
    
                return $edit;
            })
            ->make(true);
    }

    public function store(Request $request)
    {
        try {
            Purchase::insert([
                'pic' => $request->pic,
                'supplier_id' => $request->supplier_id,
                'purchase_date' => $request->purchase_date,
                'due_date' => $request->due_date,
                'receive_date' => $request->receive_date,
                'purchase_terms' => $request->purchase_terms,
                'product_id' => $request->product_id,
                'total' => $request->total,
                'memo' => $request->memo,
            ]);
            
            return redirect()->route('purchase');
        } catch (\Exception $e) {
            $status = 'error';
            $msg    = trans('label.error.internal_server_error');
            return redirect()->route('purchase')->with($status, $msg);
        }
    }
    
    public function edit(Request $request)
    {
        try {
            $model = new User;
            $user = $model->find($request->id);
            $user_id = $user->id;
            $user_email = $user->email;
            $user_nik = $user->employee_user->employee->nik;
            $user_name = $user->employee_user->employee->name;
            $user_phone = $user->employee_user->employee->phone;
            return response()->json(['id' => $user_id, 'email' => $user_email, 'nik' => $user_nik, 'name' => $user_name, 'phone' => $user_phone, 'status' => 'Success']);
        } catch (Exception $e) {      
            $status = 'error';
            $msg    = trans('label.error.internal_server_error');
            return response()->json(['status' => $status, 'message' => $msg]);
        }
    }

    public function update(Request $request)
    {
        try{
            Purchase::find($request->id)->update([
                'pic' => $request->pic,
                'supplier_id' => $request->supplier_id,
                'purchase_date' => $request->purchase_date,
                'due_date' => $request->due_date,
                'receive_date' => $request->receive_date,
                'purchase_terms' => $request->purchase_terms,
                'product_id' => $request->product_id,
                'total' => $request->total,
                'memo' => $request->memo,
            ]);
            
            return redirect()->route('purchase');
        }catch (Exception $e) {   
            \DB::rollback();   
            $status = 'error';
            $msg    = trans('label.error.internal_server_error');
            return redirect()->route('purchase')->with($status, $msg);
        }
    }
    
    public function report()
    {
        return view('backend.report.index');
    }

    public function datatableReport()
    {
        try {
            $model = new Purchase;
            $model =  $model->datatables();
        } catch (Exception $e) {
            return redirect()->route('/')->with('error', trans('label.error.internal_server_error'));
        }
        
        return DataTables::of($model)
            ->addIndexColumn()
            ->addColumn('action', function($data) {
                $edit = '<button type="button" data-url="'.$data->id.'
                " data-method="EDIT" class="btn btn-primary btn-xs" id="update-data" title="'.trans('label.update').'" style="margin-left: 5%">
                <i class="fa fa-pencil"></i></button>';
    
                return $edit;
            })
            ->make(true);
    }
    
    public function download()
    {
        $file = public_path() . "/example/template.xlsx";
        
        return response()->download($file, 'example.xlsx');
    }
}
