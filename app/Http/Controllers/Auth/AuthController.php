<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Sentinel;
use Helper;
use Reminder;
use Exception;
use Socialite;

use App\Model\User;

class AuthController extends Controller
{
    /**
     * Login page.
     * 
     * @return \Illuminate\Http\Response
    */
	public function getLogin(){
        if (user_info()) {
            $role = user_info()->roles()->get()[0];
            return redirect()->route('/');
        }
        return view('auth.login');
	}

    /**
     * Handle login request.
     *
     * @param  \App\Modules\Backend\Http\Requests\Auth\WebLoginRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        try {
            $model = User::where('email', '=', $request['email'])->first();
            if(!empty($model)){
                if(empty($model->provider_id)){
                    if($model->is_admin == 1){
                        $credentials = ['email' => $request->email, 'password' => $request->password];

                        if ($user = Sentinel::authenticate($credentials)) {
                            return redirect()->route('/');
                        } elseif (! Sentinel::findByCredentials(['login' => $request->email])) {
                            throw new Exception('These credentials do not match our records.');
                        } else {
                            throw new Exception('Password is incorrect');
                        }
                    }else{
                        return redirect()->route('/');
                    }
                }else{
                    throw new Exception('Please Login using your Social Media');
                }
            }else{
                throw new Exception('These credentials do not match our records.');
            }
        }catch (\Exception $e) {
            $e = $e->getMessage();
        }

        return back()->withInput()->with('error', $e);
    }
    
        /**
     * Handle logout request.
     * 
     * @return \Illuminate\Http\Response
     */
    public function getLogout()
    {
        if (!user_info()) {
            return redirect()->route('/');
        } else {
            $role = user_info()->roles()->get()[0];
            Sentinel::logout();
            return redirect()->route('/');
        }
    }
}
