<?php
use Illuminate\Http\Request;
use Illuminate\Contracts\Encryption\Encrypter;
use App\Model\User;
use App\Model\Zodiac;
use App\Model\Survey;

if (! function_exists('user_info')) {
    /**
     * Get logged user info.
     *
     * @param  string $column
     * @return mixed
     */
    function user_info()
    {
        if ($user = Sentinel::check()) {
            return $user;
        }

        return null;
    }
}

if (! function_exists('encrypt')) {
    /**
     * Function for encrypt string.
     *
     * @return string
     */
    function encrypt($string)
    {
        return Encrypter::encrypt($string);
    }
}

if (! function_exists('decrypt')) {
    /**
     * Function for decrypt string.
     *
     * @return string
     */
    function decrypt($string)
    {
        return Encrypter::decrypt($string);
    }
}

if (! function_exists('isSuperAdmin')) {
    /**
     * Function for check user is super-admin
     *
     * @param string
     * @return boolean
     */
    function isSuperAdmin() {
        $is_super_admin = getRoleNames()[0] == 'super-admin' ? true : false;
        return $is_super_admin;
    }
}

if (! function_exists('zodiacName')) {
    function zodiacName($id)
    {
        $month_name = Array('January','February','March','April','May','June','July','August','September','October','November','December');
        $model = User::find($id);
        $month = substr($model->birth_date, 5,2);
        $index_array = (int)$month-1;
        $date = substr($model->birth_date, 7,3);
        $date = $month.$date;
        if((('12-21' <= $date) AND ($date <= '12-31')) OR (('01-01' <= $date) AND ($date <= '01-19'))){
            $zodiac = Zodiac::find(1);
        }else{
            $zodiac = Zodiac::where('start_date','<=', $date)->where('end_date','>=', $date)->first();
        }
        $month_name = $month_name[$index_array];
        $result = ['zodiac' => $zodiac, 'month_name' => $month_name];

        return $result;
    }
}

if (! function_exists('countResult')) {
    function countResult($users, $id_question, $option)
    {
        if(empty($users)){
            $presentase = 0;
        }else{
            $count = count($users);
            $choice = 0;
            $surveys = Survey::where('question_id','=', $id_question)->get();
            foreach ($users as $user) {
                foreach ($surveys as $survey) {
                    if(($survey->user_id == $user) AND ($survey->option_id == $option)){
                        $choice++;
                    }
                }
            }
            $presentase = ($choice / $count) * 100;
        }
        return $presentase;
    }
}