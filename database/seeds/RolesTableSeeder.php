<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_users')->truncate();
        DB::table('roles')->truncate();

        Sentinel::getRoleRepository()->createModel()->create([
            'slug' => 'super-admin',
            'name' => 'Super Administrator',
            'permissions' => [
                'super_admin' => true,
                'admin' => true,
                'purchasing' => true,
                'marketing' => true,
                'sales' => true,
            ],
            'is_super_admin' => true,
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'slug' => 'supervisor',
            'name' => 'Supervisor',
            'permissions' => [
                'super_admin' => false,
                'admin' => true,
                'purchasing' => true,
                'marketing' => true,
                'sales' => true,          
            ],
            'is_super_admin' => false,
        ]);
    }
}
