<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('activations')->truncate();
        DB::table('persistences')->truncate();
        DB::table('reminders')->truncate();
        DB::table('role_users')->truncate();
        DB::table('throttle')->truncate();

        Sentinel::registerAndActivate([
            'email' => 'admin@sti.com',
            'password' => '12345678',
            'is_admin' => true,
        ]);

        Sentinel::findRoleBySlug('super-admin')->users()->attach(Sentinel::findById(1));

        Sentinel::registerAndActivate([
            'email' => 'supervisor@sti.com',
            'password' => '12345678',
            'is_admin' => true,
        ]);

        Sentinel::findRoleBySlug('supervisor')->users()->attach(Sentinel::findById(2));
        
        DB::table('employees')->truncate();
        DB::table('employee_users')->truncate();
        
        DB::table('employees')->insert([
            'nik' => '1234567812345678',
            'name' => 'Laras Mulyani',
            'photo' => null,
            'phone' => '08123123123',
        ]);
        
        DB::table('employee_users')->insert([
            'user_id' => 2,
            'employee_id' => 1,
        ]);
    }
}
