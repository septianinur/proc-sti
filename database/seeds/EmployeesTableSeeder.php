<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->truncate();
        DB::table('employee_users')->truncate();
        
        DB::table('employees')->insert([
            'nik' => '1234567812345678',
            'name' => 'Laras Mulyani',
            'photo' => null,
            'phone' => '08123123123',
        ]);
        
        DB::table('employee_users')->insert([
            'user_id' => 2,
            'employee_id' => 1,
        ]);
    }
}
