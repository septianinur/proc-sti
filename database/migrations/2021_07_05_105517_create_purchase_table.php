<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pic');
            $table->integer('supplier_id');
            $table->timestamp('purchase_date');
            $table->timestamp('due_date');
            $table->timestamp('receive_date');
            $table->string('purchase_terms')->nullable();
            $table->integer('product_id');
            $table->integer('total');
            $table->string('memo')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
