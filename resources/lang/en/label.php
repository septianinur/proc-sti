<?php

return [
    'add_form'          => 'Add Form',
    'update_form'       => 'Update Form',
    'confirm_destroy'   => 'Confirm Delete',
    'close'             => 'Close',
    'save'              => 'Save',
    'destroy'           => 'Delete',
    'update'			=> 'Update',
    'view'				=> 'View',
	'table' => [
		'id'			=> 'ID',
		'number'		=> 'No',
		'question'		=> 'Question',
		'option'		=> 'Option',
	    'created_at'	=> 'Created At',
	    'action'		=> 'Action',
		'position'		=> 'Position',
		'email'			=> 'Email',
		'first_name'	=> 'Name',
		'birth_date'	=> 'Birth Date',
	],
	'error' => [
		'internal_server_error'		=> 'Internal Server Error',
	],
	'success' => [
		'destroy'		=> 'Delete Successfull',
		'update'		=> 'Update Successfull',
	],
];