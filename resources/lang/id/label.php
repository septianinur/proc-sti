<?php

return [
    'add_form'          => 'Tambah Form',
    'update_form'       => 'Ubah Form',
    'confirm_destroy'   => 'Konfirmasi Untuk Menghapus',
    'close'             => 'Tutup',
    'save'              => 'Simpan',
    'destroy'           => 'Hapus',
    'update'			=> 'Ubah',
    'view'				=> 'View',
	'table' => [
		'id'			=> 'ID',
		'number'		=> 'No',
		'question'		=> 'Question',
		'option'		=> 'Option',
	    'created_at'	=> 'Created At',
	    'action'		=> 'Action',
		'position'		=> 'Posisi',
		'email'			=> 'Email',
		'first_name'	=> 'Nama',
		'birth_date'	=> 'Tanggal Lahir',
	],
	'error' => [
		'internal_server_error'		=> 'Internal Server Error',
	],
	'success' => [
		'save'  		=> 'Berhasil Menyimpan Data',
		'destroy'		=> 'Berhasil Menghapus Data',
		'update'		=> 'Berhasil Mengubah Data',
	],
];