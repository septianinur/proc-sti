<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="robots" content="noindex, nofollow">
        <meta name="product">
        <meta name="description">
        <meta name="author" content="Ajid Muhamad">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <title>STI - Sign In</title>
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/AdminLTE.min.css')}}">
    </head>
    
    <body class="login-page">
        <div class="login-box">
            <div class="login-logo">
                <b>Eprocurement</b> STI</a>
            </div>
            @if(session()->has('error'))
          		<div class="alert alert-danger alert-dismissible alert-dashboard">
              		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              		{{session()->get('error')}}
          		</div>
          	@endif
            <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>
                <p class="login-box-msg"></p>
                {!! Form::open(['route' => 'login.post', 'role' => 'form', 'autocomplete'=>'off']) !!}
                    <div class="form-group  has-feedback">
                        <input class="form-control" placeholder="Email" name="email" type="text" value="{{old('email')}}">
                        <div class="text-danger">{!! $errors->first('email') !!}</div>
                    </div>
                    <div class="form-group  has-feedback">
                        <input class="form-control" placeholder="Password" name="password" type="password" value="">
                        <div class="text-danger">{!! $errors->first('password') !!}</div>             
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                        </div>
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign in</button>
                        </div>
                    </div>
                {!! Form::close() !!} 
            </div>
        </div>
        <script src="{{asset('assets/js/jquery.min.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    </body>
</html>
