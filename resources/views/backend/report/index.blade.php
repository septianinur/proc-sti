@extends('backend.layout.master')

@section('header')
    <link href="{{asset('assets/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Report</li>
    </ol>
@endsection

@section('content')
    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible alert-dashboard">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{session()->get('error')}}
        </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-md-7">
                    <h3 class="box-title">Report List</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <button class="btn btn-success" id="add-data"><span class="fa fa-plus"></span> CREATE</button>
                </div>
            </div>
        </div>
        <div class="box-body">
            <table id="user-list" class="table table-bordered table-striped table-responsive" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>start_at</th>
                        <th>end_at</th>
                        <th>created_at</th>
                        <th>action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
    <div class="modal fade" id="modal-save">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4>Create Form</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(['url' => route('purchase.store'), 'method'=>'POST', 'class' => 'modal-form-save']) }}
                    
                    <div class="form-group">
                        <input type="text" class="form-control" id="purchase.save.pic" name="pic" placeholder="Start Date">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="purchase.save.supplier" name="supplier" placeholder="End Date">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-update">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4>Update Form</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(['url' => route('purchase.update'), 'method'=>'POST', 'class' => 'modal-form-update']) }}
                    <input type="hidden" name="id" id="purchase.update.id">
                    
                    <div class="form-group">
                        <input type="text" class="form-control" id="purchase.update.pic" name="pic" placeholder="pic">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="purchase.update.supplier" name="supplier" placeholder="supplier">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="purchase.update.purchase" name="purchase" placeholder="purchase">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="purchase.update.due" name="due" placeholder="due date">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="purchase.update.receive" name="receive" placeholder="receive date">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="purchase.update.purchase" name="purchase" placeholder="purchase date">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="purchase.update.product" name="product" placeholder="product">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="purchase.update.total" name="total" placeholder="total">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="purchase.update.memo" name="memo" placeholder="memo">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @include('backend.report.script')
@endsection