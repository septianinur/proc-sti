<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script>
    var user_datatable = $('#user-list').DataTable({
        processing: true,
        serverSide: true,
        bInfo: false,
        ajax: '{{ route("product.datatables") }}',
        columns: [
            {data:'id',orderable:false},
            {data:'name', name:'name',},
            {data:'satuan', name:'satuan',},
            {data:'category_id', name:'category_id',},
            {data:'supplier_id', name:'supplier_id',},
            {data:'created_at', name:'created_at', },
            {data:'action', name:'action', },
        ],
        columnDefs:
        [{
            targets : 0,
            orderable : false,
            render : function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }],
    });

</script>