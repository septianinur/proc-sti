@extends('backend.layout.master')

@section('header')
    <link href="{{asset('assets/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Supplier</li>
    </ol>
@endsection

@section('content')
    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible alert-dashboard">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{session()->get('error')}}
        </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-md-7">
                    <h3 class="box-title">Supplier List</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <button class="btn btn-success" id="add-data"><span class="fa fa-plus"></span> CREATE</button>
                </div>
            </div>
        </div>
        <div class="box-body">
            <table id="user-list" class="table table-bordered table-striped table-responsive" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>address</th>
                        <th>email</th>
                        <th>phone</th>
                        <th>tax_number</th>
                        <th>bank_code</th>
                        <th>bank_name</th>
                        <th>account_number</th>
                        <th>created_by</th>
                        <th>created_at</th>
                        <th>action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
@endsection

@section('footer')
    @include('backend.supplier.script')
@endsection