<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script>
    var user_datatable = $('#user-list').DataTable({
        processing: true,
        serverSide: true,
        bInfo: false,
        ajax: '{{ route("supplier.datatables") }}',
        columns: [
            {data:'name', name:'name', },
            {data:'address', name:'address', },
            {data:'email', name:'email', },
            {data:'phone', name:'phone', },
            {data:'tax_number', name:'tax_number', },
            {data:'bank_code', name:'bank_code', },
            {data:'bank_name', name:'bank_name', },
            {data:'account_number', name:'account_number', },
            {data:'created_by', name:'created_by', },
            {data:'created_at', name:'created_at', },
            {data:'action', name:'action', },
        ],
        columnDefs:
        [{
            targets : 0,
            orderable : false,
            render : function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }],
    });

</script>