<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script>
    var user_datatable = $('#user-list').DataTable({
        processing: true,
        serverSide: true,
        bInfo: false,
        ajax: '{{ route("purchase.datatables") }}',
        columns: [
            {data:'id',orderable:false},
            {data:'pic', name:'pic',},
            {data:'supplier', name:'supplier',},
            {data:'purchase', name:'purchase',},
            {data:'due', name:'due',},
            {data:'receive', name:'receive',},
            {data:'purchase', name:'purchase',},
            {data:'product', name:'product',},
            {data:'total', name:'total',},
            {data:'memo', name:'memo',},
            {data:'status', name:'status',},
            {data:'created_at', name:'created_at',},
            {data:'action', name:'action',},
        ],
        columnDefs:
        [{
            targets : 0,
            orderable : false,
            render : function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }],
    });

    $(document).on('click', '#add-data', function() {
        $('#modal-save').modal('show');
    });
    
    $(document).on('click', '#update-data', function() {
        $.ajax({
            url : '{{route("purchase.edit")}}',
            type : 'GET',
            dataType : 'json',
            data : {
                id : $(this).data('url')
            },
            success : function(data){
                $('#purchase.update.id').val(data['id']);
                $('#purchase.update.pic').val(data['pic']);
                $('#purchase.update.supplier').val(data['supplier']);
                $('#purchase.update.purchase').val(data['purchase']);
                $('#purchase.update.due').val(data['due']);
                $('#purchase.update.receive').val(data['receive']);
                $('#purchase.update.purchase').val(data['purchase']);
                $('#purchase.update.product').val(data['product']);
                $('#purchase.update.total').val(data['total']);
                $('#purchase.update.memo').val(data['memo']);
                $('#modal-update').modal('show');
            },
            error : function (xhr, status){
            },
            complete : function(){
                alreadyloading = false;
            }
        });
    });

</script>