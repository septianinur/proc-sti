<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
            <div class="pull-left image">
            	<img src="{{asset('assets/img/c_avatar5.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>
                    {{ user_info()->name }} {{ user_info()->name }}
                </p>
            	<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            	<li>
                	<a href="{{route('/')}}">
                  		<i class="fa fa-dashboard"></i> <span>Dashboard</span>
                      	<span class="pull-right-container"></span>
                    </a>
                </li>
                <li>
                    <a href="{{route('user')}}">
                        <i class="fa fa-user"></i> <span>Users</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('product')}}">
                	    <i class="fa fa-question-circle"></i> <span>Product Management</span>
                    </a>
				</li>
                <li>
                    <a href="{{route('supplier')}}">
                	    <i class="fa fa-question-circle"></i> <span>Supplier Management</span>
                    </a>
				</li>
                <li>
                    <a href="{{route('purchase')}}">
                	    <i class="fa fa-question-circle"></i> <span>Purchase Management</span>
                    </a>
				</li>
                <li>
                    <a href="{{route('report')}}">
                	    <i class="fa fa-question-circle"></i> <span>Report</span>
                    </a>
				</li>
			</li>
        </ul>
    </section>
</aside>