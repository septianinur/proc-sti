@extends('backend.layout.master')

@section('header')
    <link href="{{asset('assets/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Users</li>
    </ol>
@endsection

@section('content')
    @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible alert-dashboard">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{session()->get('error')}}
        </div>
    @endif
    <div class="box box-primary">
        <div class="box-header">
            <div class="row">
                <div class="col-md-7">
                    <h3 class="box-title">Users List</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <button class="btn btn-success" id="add-data"><span class="fa fa-plus"></span> CREATE</button>
                </div>
            </div>
        </div>
        <div class="box-body">
            <table id="user-list" class="table table-bordered table-striped table-responsive" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NIK</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>No Telpon</th>
                        <th>created_at</th>
                        <th>action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
    <div class="modal fade" id="modal-save">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4>Create Form</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(['url' => route('user.store'), 'method'=>'POST', 'class' => 'modal-form-save']) }}
                    <div class="form-group">
                        <input type="text" class="form-control" id="user.save.nik" name="nik" placeholder="NIK">
                        <div class="text-validation">{!! $errors->first('nik') !!}</div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="user.save.name" name="name" placeholder="Nama">
                        <div class="text-validation">{!! $errors->first('name') !!}</div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="user.save.email" name="email" placeholder="Email">
                        <div class="text-validation">{!! $errors->first('email') !!}</div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="user.save.phone" name="phone" placeholder="No Telpon">
                        <div class="text-validation">{!! $errors->first('phone') !!}</div>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="user.save.password" name="password" placeholder="Password">
                        <div class="text-validation">{!! $errors->first('password') !!}</div>
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="user.save.role" name="role">
                            <option value="" disabled selected> PILIH ROLE USER </option>
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-update">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4>Update Form</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(['url' => route('user.update'), 'method'=>'POST', 'class' => 'modal-form-update']) }}
                    <input type="hidden" name="id" id="user.update.id">
                    
                    <div class="form-group">
                        <input type="text" class="form-control" id="user.update.nik" name="nik" placeholder="NIK">
                        <div class="text-validation">{!! $errors->first('nik') !!}</div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="user.update.name" name="name" placeholder="Nama">
                        <div class="text-validation">{!! $errors->first('name') !!}</div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="user.update.email" name="email" placeholder="Email">
                        <div class="text-validation">{!! $errors->first('email') !!}</div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="user.update.phone" name="phone" placeholder="No Telpon">
                        <div class="text-validation">{!! $errors->first('phone') !!}</div>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="user.update.password" name="password" placeholder="Password">
                        <div class="text-validation">{!! $errors->first('password') !!}</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @include('backend.user.script')
@endsection