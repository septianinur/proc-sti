<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script>
    var user_datatable = $('#user-list').DataTable({
        processing: true,
        serverSide: true,
        bInfo: false,
        ajax: '{{ route("user.datatables") }}',
        columns: [
            {data:'id',orderable:false},
            {data:'nik', name:'nik', },
            {data:'name', name:'name', },
            {data:'email', name:'email', },
            {data:'phone', name:'phone', },
            {data:'created_at', name:'created_at', },
            {data:'action', name:'action', },
        ],
        columnDefs:
        [{
            targets : 0,
            orderable : false,
            render : function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }],
    });

    $(document).on('click', '#add-data', function() {
        $('#modal-save').modal('show');
    });
    
    $(document).on('click', '#update-data', function() {
        $.ajax({
            url : '{{route("user.edit")}}',
            type : 'GET',
            dataType : 'json',
            data : {
                id : $(this).data('url')
            },
            success : function(data){
                $('#user.update.id').val(data["id"]);
                $('#user.update.email').val(data['email']);
                $('#user.update.nik').val(data['nik']);
                $('#user.update.name').val(data['name']);
                $('#user.update.phone').val(data['phone']);
                $('#modal-update').modal('show');
            },
            error : function (xhr, status){
            },
            complete : function(){
                alreadyloading = false;
            }
        });
    });

</script>